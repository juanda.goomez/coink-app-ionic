import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { environmentConstants } from '../../../environment/envoronment.const';
import { CryptProvider } from '../../../providers/crypt/crypt';
import { ApiRestProvider } from '../../../providers/api-rest/api-rest';
import { environmentApi } from '../../../environment/environment.api';
import { AlertMessagesProvider } from '../../../providers/alert-messages/alert-messages';

/**
 * Generated class for the RegisterStep1PhonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-step1-phone',
  templateUrl: 'register-step1-phone.html',
})
export class RegisterStep1PhonePage {

  public phone: string = '';
  public code: string = '';
  public check: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events, public crypt: CryptProvider, public apiService: ApiRestProvider, public loadingCtrl: LoadingController, public alertMessages: AlertMessagesProvider) {
    this.subscribeToEvents();
  }



  ionViewDidLeave() {
    this.phone = '';
    this.code = '';
    this.check = false;
    this.events.unsubscribe(environmentConstants.PRESS_KEY);
  }

  subscribeToEvents() {
    this.events.subscribe(environmentConstants.PRESS_KEY, (result) => {
      if (this.check) {
        this.addCode(result);
      } else {
        this.addPhone(result);
      }

    });
  }


  addCode(result) {
    if (result === 'right') {
      if (this.code.length === 4) {
        this.events.publish(environmentConstants.PHONE_CHECK);
        this.check = false;
        this.phone = '';
        this.code = '';
        this.events.publish(environmentConstants.CODE_CHECK);
      }
    }
    else if (result === 'left') {
      this.events.publish(environmentConstants.PHONE_CHECK);
      this.code = this.code.slice(0, -1);
    }
    else {
      if (this.code.length === 3) {
        this.code = this.code.concat(result);
        this.events.publish(environmentConstants.PHONE_MAX);
      }
      else if (this.code.length < 4) {
        this.code = this.code.concat(result);
      }
    }
  }

  addPhone(result) {
    if (result === 'right') {
      if (this.phone.length === 11) {
        this.phone = this.remove_character('-', this.phone);
        const json = {
          "phone_number": '57'.concat(this.phone),
          "imei": "8888888888888"
        }
        const result = this.crypt.encrypt(JSON.stringify(json));
        this.checkPhone(result);
      }
    }
    else if (result === 'left') {
      this.phone = this.phone.slice(0, -1);
      this.events.publish(environmentConstants.PHONE_CHECK);
    }
    else {
      if (this.phone.length === 10) {
        this.phone = this.phone.concat(result);
        this.events.publish(environmentConstants.PHONE_MAX);
      }
      else if (this.phone.length === 3) {
        this.phone = this.phone.concat('-');
        this.phone = this.phone.concat(result);

      } else if (this.phone.length < 10) {
        this.phone = this.phone.concat(result);
      }
    }
  }

  remove_character(str_to_remove, str) {
    let reg = new RegExp(str_to_remove)
    return str.replace(reg, '')
  }

  checkPhone(body) {
    let loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    loading.present();
    const json = {
      "payload": body
    }
    const params = {
      "apiKey": environmentApi.API_KEY_PARAM
    }
    this.apiService.post(environmentApi.verifyDirectLogin, json, params).then(result => {
    }).catch(error => {
      if (error.status === 404) {
        this.check = true;
        this.alertMessages.presentAlert("Mensaje enviado", "Verifica tus mensajes entrantes e ingresa el código", "Confirmar")
        loading.dismiss();
        this.events.publish(environmentConstants.PHONE_CHECK);
      }
      else if (error.status === 400) {
        this.alertMessages.presentAlert("Error", "El número ".concat(this.phone).concat(" no se encuentra en el formato correcto"), "Volver")
        this.events.publish(environmentConstants.PHONE_CHECK);
        loading.dismiss();
        this.phone = '';
      }
      else {
        this.alertMessages.presentAlert("Error de conexión", "Revise su conexión a internet e intente de nuevo", "Volver")
        loading.dismiss();
      }

    })
  }


}
