import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterStep1PhonePage } from './register-step1-phone';
import { IonDigitKeyboard } from '../../../components/ion-digit-keyboard/ion-digit-keyboard.module';
@NgModule({
  declarations: [
    RegisterStep1PhonePage,
  ],
  imports: [
    IonDigitKeyboard,
    IonicPageModule.forChild(RegisterStep1PhonePage),
  ],
  exports: [
   RegisterStep1PhonePage
  ],

})
export class RegisterStep1PhonePageModule {}
