import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import { UserModel } from '../../../models/user';
import { environmentConstants } from '../../../environment/envoronment.const';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertMessagesProvider } from '../../../providers/alert-messages/alert-messages';

/**
 * Generated class for the RegisterStep3ConfigAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-step3-config-account',
  templateUrl: 'register-step3-config-account.html',
})
export class RegisterStep3ConfigAccountPage implements OnInit {

  public registerFormGroup: FormGroup;
  public user: UserModel = new UserModel();
  public confirmEmail: string;
  public confirmPin: string;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    private formBuilder: FormBuilder,
    public alertMessage:AlertMessagesProvider
  ) {
  }

  ngOnInit() {
    this.configForm()
  }

  configForm() {
    this.registerFormGroup = this.formBuilder.group({
      email: [{ value: '', disabled: false }, [Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      emailConfirmation: [{ value: '', disabled: false }, [Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
      pin: [{ value: '', disabled: false }, [Validators.required]],
      pinConfirmation: [{ value: '', disabled: false }, [Validators.required]]
    });
  }

  nextStep() {
    this.events.publish(environmentConstants.NEXT_STEP);
  }

  logForm() {
    if (this.registerFormGroup.invalid) {
      return;
    }
    if(this.user.email===this.confirmEmail && this.user.pin===this.confirmPin){
      this.nextStep();
      this.registerFormGroup.reset();
    }
    else
      this.alertMessage.presentAlert("Error de confirmación","El email o el pin que ingresaste no coinciden","Aceptar");
  }

 
}
