import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterStep3ConfigAccountPage } from './register-step3-config-account';

@NgModule({
  declarations: [
    RegisterStep3ConfigAccountPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterStep3ConfigAccountPage),
  ],
  exports: [
    RegisterStep3ConfigAccountPage,
  ],
})
export class RegisterStep3ConfigAccountPageModule {}
