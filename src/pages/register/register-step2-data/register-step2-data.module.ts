import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterStep2DataPage } from './register-step2-data';

@NgModule({
  declarations: [
    RegisterStep2DataPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterStep2DataPage),
  ],
  exports: [
    RegisterStep2DataPage,
  ],
})
export class RegisterStep2DataPageModule {}
