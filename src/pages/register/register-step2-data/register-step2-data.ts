import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController, AlertController } from 'ionic-angular';
import { UserModel } from '../../../models/user';
import { environmentConstants } from '../../../environment/envoronment.const';
import { environmentApi } from '../../../environment/environment.api';
import { AlertMessagesProvider } from '../../../providers/alert-messages/alert-messages';
import { ApiRestProvider } from '../../../providers/api-rest/api-rest';
import { CryptProvider } from '../../../providers/crypt/crypt';

/**
 * Generated class for the RegisterStep2DataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-step2-data',
  templateUrl: 'register-step2-data.html',
})
export class RegisterStep2DataPage implements OnInit {

  registerFormGroup: FormGroup;
  user: UserModel = new UserModel();
  success: boolean = false;
  data: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    private formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    public alertMessages: AlertMessagesProvider,
    public apiService: ApiRestProvider,
    public crypt: CryptProvider,
    private alertCtrl: AlertController
  ) {
  }

  ngOnInit() {
    this.configForm()
  }

  nextStep() {
    this.success=false;
    this.registerFormGroup.reset();
    this.events.publish(environmentConstants.NEXT_STEP);
  }

  configForm() {
    this.registerFormGroup = this.formBuilder.group({
      documentType: [{ value: '', disabled: false }, [Validators.required]],
      documentNumber: [{ value: '', disabled: false }, [Validators.required]],
      birthDate: [{ value: '', disabled: false }, [Validators.required]],
      expeditionDate: [{ value: '', disabled: false }, [Validators.required]],
      gender: [{ value: '', disabled: false }, [Validators.required]],
    });
  }

  inputValidation(formControlName, validation, formGroupTo) {
    return (formGroupTo.get(formControlName).hasError(validation)
      && formGroupTo.get(formControlName).touched);
  }

  logForm() {
    if (this.registerFormGroup.invalid) {
      alert();
      return;
    }
    const body = this.crypt.encrypt(JSON.stringify(this.user));
    this.checkData(body);
  }

  checkData(body) {
    let loading = this.loadingCtrl.create({
      content: 'Por favor espere...'
    });
    loading.present();
    const json = {
      "payload": body
    }
    const params = {
      "apiKey": environmentApi.API_KEY_PARAM
    }
    this.apiService.post(environmentApi.cifin, json, params).then(result => {
      this.data = result;
      loading.dismiss();
      this.success = true;
      this.alertMessages.presentAlert("Datos encontrados", "Por favor verifica tu información", "Confirmar")

    }).catch(error => {
      console.log(error);
      if (error.status === 400) {
        this.alertMessages.presentAlert("Error", "Los datos enviados no son correctos", "Volver")
        loading.dismiss();
      }
      else{
        this.alertMessages.presentAlert("Error de conexión", "Revise su conexión a internet e intente de nuevo", "Volver")
        loading.dismiss();
      }

    })
  }


  alertCancel() {
    let alert = this.alertCtrl.create({
      title: '¿Estas seguro?',
      message: 'Si cancelas la verificación,tu progreso se perderá y tendrás que volver a empezar',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            this.navCtrl.push('TutorialPage');
          }
        },
        {
          text: 'Continuar',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }


}
