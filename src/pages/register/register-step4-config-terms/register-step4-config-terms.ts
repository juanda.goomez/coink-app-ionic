import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegisterStep4ConfigTermsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register-step4-config-terms',
  templateUrl: 'register-step4-config-terms.html',
})
export class RegisterStep4ConfigTermsPage {
   check=false;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

 goToHome(){
  this.navCtrl.push('TutorialPage');
 }
}
