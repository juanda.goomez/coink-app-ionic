import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterStep4ConfigTermsPage } from './register-step4-config-terms';

@NgModule({
  declarations: [
    RegisterStep4ConfigTermsPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterStep4ConfigTermsPage),
  ],
  exports: [
    RegisterStep4ConfigTermsPage,
  ],
})
export class RegisterStep4ConfigTermsPageModule {}
