import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';

import { RegisterPage } from './register';
import { IonicStepperModule } from 'ionic-stepper';
import { RegisterStep1PhonePageModule } from './register-step1-phone/register-step1-phone.module';
import { IonDigitKeyboard } from '../../components/ion-digit-keyboard/ion-digit-keyboard.module';
import { RegisterStep2DataPageModule } from './register-step2-data/register-step2-data.module';
import { RegisterStep4ConfigTermsPageModule } from './register-step4-config-terms/register-step4-config-terms.module';
import { RegisterStep3ConfigAccountPageModule } from './register-step3-config-account/register-step3-config-account.module';

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    IonicStepperModule,
    IonDigitKeyboard,
    RegisterStep1PhonePageModule,
    RegisterStep2DataPageModule,
    RegisterStep3ConfigAccountPageModule,
    RegisterStep4ConfigTermsPageModule,
    IonicPageModule.forChild(RegisterPage),
    TranslateModule.forChild(),
  ],
  exports: [
    RegisterPage
  ]
})
export class RegisterPageModule { }
