import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Events } from 'ionic-angular';
import { environmentConstants } from '../../environment/envoronment.const';
import { IonicStepperComponent } from 'ionic-stepper';

/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  action: string = 'NÚMERO CELULAR';
  step:number=1;
  checkPhone=false;

  @ViewChild('stepper') stepRef: IonicStepperComponent;
  constructor(public navCtrl: NavController, public events: Events) {
    this.subscribeToEvents();
  }


  subscribeToEvents() {

    this.events.subscribe(environmentConstants.CODE_CHECK, () => {
      this.step++;   
      this.checkStep();     
      this.stepRef.nextStep();      
    });
    this.events.subscribe(environmentConstants.NEXT_STEP, () => {
      this.step++;
      this.checkStep(); 
      this.stepRef.nextStep();   
    });
  }

  checkStep(){
    if(this.step===1){
      this.action='NÚMERO CELULAR'
    }
    if(this.step===2){
      this.action='DATOS DE TU CUENTA'
    }
    else if(this.step===3){
      this.action='SEGURIDAD'
    }
   else  if(this.step===4){
      this.action='AUTORIZACION DE DATOS'
    }
    
   
  }

  goBack() {
    if(this.step===1){
      this.navCtrl.push('TutorialPage');

    }else{
      this.step--;
      this.checkStep();
      this.stepRef.previousStep();
    }
  }

  selectChange(e) {
    console.log(e);
  }




}
