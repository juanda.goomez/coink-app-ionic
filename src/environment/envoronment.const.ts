export const environmentConstants = {
    //Events    
    PRESS_KEY: 'pressKey',
    NEXT_STEP: 'nextStep',
    PHONE_MAX: 'phoneMax',
    PHONE_CHECK: 'phoneCheck',
    CODE_CHECK: 'codeCheck',


    TUTORIAL_SLIDE1_TITLE: "<b>LA ALCANCÍA QUE SIEMPRE<br>TE ACOMPAÑA</b>",
    TUTORIAL_SLIDE1_DESCRIPTION: "Desliza para aprender más",
  
    TUTORIAL_SLIDE2_TITLE: "TU MARRANO CRECE CON TUS AHORROS",
    TUTORIAL_SLIDE2_DESCRIPTION: "En la app de Coink puedes ahorrar de manera 100% digital y con funcionalidades estimulantes y divertidas.",
  
    TUTORIAL_SLIDE3_TITLE: "100% LEGAL Y SEGURO COMO UN BANCO",
    TUTORIAL_SLIDE3_DESCRIPTION: "Coink es una entidad vigilada por la Superintendencia Financiera de Colombia y el dinero de nuestros usuarios está asegurado por Fogafin.",
  
  };
  