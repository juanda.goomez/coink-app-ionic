export class UserModel {
name:string;
document_number:string;
document_type:string;
gender:string;
birth_date:Date;
expedition_date:Date;
email:string;
pin:string;

constructor(){
    this.name='';
    this.document_number='';
    this.document_type='';
    this.gender='';
    this.email='';
    this.pin='';
    
}

}