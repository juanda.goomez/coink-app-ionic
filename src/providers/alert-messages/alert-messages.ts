import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

/*
  Generated class for the AlertMessagesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AlertMessagesProvider {

  constructor(private alertCtrl: AlertController) {
  }


  presentAlert(title,subTitle,textButton) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: [  {
        text: textButton,   
      },]
    });
    alert.present();
  } 


}
