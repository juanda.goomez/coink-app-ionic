import { Injectable } from '@angular/core';
import { environmentApi } from '../../environment/environment.api';
import * as CryptoJS from 'crypto-js';
/*
  Generated class for the CryptProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CryptProvider {
  
  key=environmentApi.KEY;
  enc=CryptoJS.enc;
  constructor() {
  }


  encrypt(serialized_json:string){
    const topEncryptedArray=this.enc.Utf8.parse(serialized_json);
    const keyHash=this.getHashKey();
    const pad = CryptoJS.pad.Pkcs7;
    const mode = CryptoJS.mode.ECB;
    const payload=CryptoJS.TripleDES.encrypt(topEncryptedArray, keyHash, { mode:mode, padding:pad });
    return payload.ciphertext.toString(this.enc.Base64);
  }


  private getHashKey(){
       let securityKeyArray= CryptoJS.MD5(this.key).toString();
       securityKeyArray+=securityKeyArray.substring(0,16);
       return this.enc.Hex.parse(securityKeyArray);
  }

}
