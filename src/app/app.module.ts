import {  HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule} from '@ionic/storage';

import { IonicApp, IonicErrorHandler, IonicModule, Keyboard } from 'ionic-angular';

import { MyApp } from './app.component';
import { IonicStepperModule } from 'ionic-stepper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonDigitKeyboard } from '../components/ion-digit-keyboard/ion-digit-keyboard.module';
import { ApiRestProvider } from '../providers/api-rest/api-rest';
import { CryptProvider } from '../providers/crypt/crypt';
import { AlertMessagesProvider } from '../providers/alert-messages/alert-messages';




@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonDigitKeyboard,
    BrowserAnimationsModule,
    IonicStepperModule,  
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    SplashScreen,
    StatusBar,    
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ApiRestProvider,
    CryptProvider,
    Keyboard,
    AlertMessagesProvider
  ]
})
export class AppModule { }
